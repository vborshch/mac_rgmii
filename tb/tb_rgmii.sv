
module tb_rgmii;
  // ******************************************
  // CLOCK GENERATOR***************************
  timeunit 1ns;
  timeprecision 1ns;

  logic           iclkin    ;
  logic           iclkout   ;
  logic           iclk      ;
  logic           iclk_tx   ;
  logic           iphy_clk  ;

  initial     forever #5.2 iclkin   = ~iclkin;
  initial     forever #5.2 iclkout  = ~iclkout;
  initial     forever #20  iclk     = ~iclk;     // 25 MHz
  initial #10 forever #20  iclk_tx  = ~iclk_tx;  // 25 MHz 90*
  initial #15 forever #20  iphy_clk = ~iphy_clk;// 25 MHz + jitter
  // ******************************************
  // ******************************************
  
  //--------------------------------------------------------------------------------------------------
  // Declaration parameters
  //--------------------------------------------------------------------------------------------------
  
  localparam int pPACKETS = 128;

  //--------------------------------------------------------------------------------------------------
  // Declaration variables
  //--------------------------------------------------------------------------------------------------

  logic            irst     ;
  logic            isop     ;
  logic            iena     ;
  logic      [7:0] idat     ;
  logic            ieop     ;
  logic            osop     ;
  logic            oena     ;
  logic      [7:0] odat     ;
  logic            oeop     ;
  logic            otx_rdy  ;
  logic            otx_busy ;
  logic            iphy_nint;
  logic            iphy_ena ;
  logic      [3:0] iphy_dat ;
  logic            ophy_ena ;
  logic      [3:0] ophy_dat ;
  logic            ophy_clk ;
  logic            nint     ;
  logic            mdc      ;
  logic            reset    ;
  logic            cfg      ;
  wire             mdio     ;
  wire             ptp      ;

  logic   [7:0] arr_int [pPACKETS][];
  logic   [7:0] arr_ext [pPACKETS][];
  logic   [7:0] rxarr[$];
  int           iter_int = 0, iter_ext = 0;
  logic         bsy;
  logic   [7:0] int_rx_arr[$];
  logic   [7:0] ext_rx_arr[$];
  logic   [7:0] dat;
  logic         cnt;
  logic         ena, ext_ena, eop;

  //--------------------------------------------------------------------------------------------------
  // Initialization
  //--------------------------------------------------------------------------------------------------

  initial begin
    for (int i = 0; i < pPACKETS; i++) begin
      automatic int size = 63 + $urandom_range(0, 5);
      for (int ii = 0; ii < size; ii++) begin
        arr_int[i] = {arr_int[i], 0};
        arr_ext[i] = {arr_ext[i], 0};
        //arr[i] = i;
      end
    end
  end
  
  //--------------------------------------------------------------------------------------------------
  // BODY
  //--------------------------------------------------------------------------------------------------

  assign isop = osop;
  assign iena = oena;
  assign idat = odat;
  assign ieop = oeop;


  // Main thread
  initial begin
    #1us;
    // Half-duples
    // int_test(pPACKETS); #10us; ext_test(pPACKETS);
    // Full-duplex
    fork
      // int_test(pPACKETS);
      ext_test(pPACKETS);
    join_none
  end
/*
  // Thread of ETH tx driver
  task int_test(int niter);
    // Randomization
    for (int i = 0; i < niter; i++) begin
      void'(rand_pack(arr_int[i]));
      arr_int[i] = {i[7:0], arr_int[i]};
    end
    // TX
    for (iter_int = 0; iter_int < niter; iter_int++) begin
      wait(otx_rdy == 1'b1);
      @(posedge iclkin);
      isop = 1'b1; @(posedge iclkin); isop = 1'b0;

      for (int j = 0; j < $size(arr_int[iter_int]); j++) begin
        if (otx_rdy == 1'b1)  iena = 1'b1;
        else begin
          iena = 1'b0;
          while(otx_rdy == 1'b0) begin
            #1us;
          end
          iena = 1'b1;
        end

        if (j == $size(arr_int[iter_int]) - 1) begin
          ieop = 1'b1;
        end
        //
        idat = arr_int[iter_int][j];
          @(posedge iclkin);
      end
      iena = 1'b0;
      ieop = 1'b0;
      //
      #10us;
    end
  endtask : int_test
*/
  // Thread of PHY tx driver
  task ext_test(int niter);
    // Randomization
    for (int i = 0; i < niter; i++) begin
      void'(rand_pack(arr_ext[i]));
      arr_ext[i] = {i[7:0], arr_ext[i]};
      arr_ext[i] = {8'h55, 8'h55, 8'h55, 8'h55, 8'h55, 8'h55, 8'hD5, arr_ext[i]};
    end
    // TX
    for (iter_ext = 0; iter_ext < niter; iter_ext++) begin
      @(posedge iphy_clk);
      iphy_ena = 1'b1;
      //
      for (int j = 0; j < $size(arr_ext[iter_ext]); j++) begin
        iphy_dat = arr_ext[iter_ext][j][3:0];
        @(posedge iphy_clk);
        iphy_dat = arr_ext[iter_ext][j][7:4];
        @(posedge iphy_clk);
      end
      iphy_ena = 1'b0;
      //
      repeat(12)@(posedge iphy_clk);
    end
  endtask : ext_test
/*
  // External reader thread
  always@(posedge iclkin) begin
    eop <= oeop;
    if (oena) ext_rx_arr.push_back(odat);

    if (eop) begin
      rxarr = arr_ext[ext_rx_arr[0]]; rxarr = rxarr[7:$]; // preamble delete
      $display("TX number: %4.0d; RX number: %4.0d; Packet size: %8.0d", iter_ext-1, rxarr[0], ext_rx_arr.size());
      if (rxarr != ext_rx_arr) begin
        $display("External test: Bad packet:");
        foreach (rxarr[i]) begin
          if (rxarr[i] != ext_rx_arr[i])  $display("TX: 0x%X RX: 0x%X ^", rxarr[i], ext_rx_arr[i]);
          else                            $display("TX: 0x%X RX: 0x%X"  , rxarr[i], ext_rx_arr[i]);
        end
        $stop();
      end
      else $display("External test: GOOD packet");
      //
      rxarr.delete();
      ext_rx_arr.delete();
    end
  end

  // Internal reader thread
  int    rx_iter = 0;

  always@(posedge ophy_clk) begin
    if (ophy_ena) cnt <= cnt + 1'b1;
    else          cnt <= '0;
    ena <= ophy_ena;
    dat <= {ophy_dat, dat[7:4]};
  end
  always@(negedge ophy_clk) if (ena & cnt) int_rx_arr.push_back(dat);

  initial forever begin
    @(posedge ophy_clk iff (top__.tx__.cnt_pause == 'd11));
    //
    // for (int i = 0; i < 9; i++) begin // preamble delete
      repeat(9) begin
        void'(int_rx_arr.pop_front());
      // $display("0x%X", int_rx_arr.pop_front());
    end
    // end
    // int_rx_arr = int_rx_arr[9:$high(int_rx_arr)];
    $display("TX number: %4.0d; RX number: %4.0d;", rx_iter, int_rx_arr[0]);
    $display("TX size: %4.0d; RX size: %8.0d", arr_int[rx_iter].size(), int_rx_arr.size());
    if (arr_int[rx_iter] != int_rx_arr) begin
      $display("Internal test: Bad packet:");
      // foreach (arr_int[rx_iter][i]) begin
      foreach (int_rx_arr[i]) begin
        if (arr_int[rx_iter][i] != int_rx_arr[i])  $display("TX: 0x%X RX: 0x%X ^", arr_int[rx_iter][i], int_rx_arr[i]);
        else                                       $display("TX: 0x%X RX: 0x%X"  , arr_int[rx_iter][i], int_rx_arr[i]);
      end
      $stop();
    end
    else $display("Internal test: GOOD packet");
    //
    rx_iter++;
    int_rx_arr.delete();
  end
*/
  function rand_pack(inout logic [7:0] arr []);
    foreach(arr[i]) begin
      arr[i] = $urandom_range(0, 255);
    end
    //
    return 0;
  endfunction


  //--------------------------------------------------------------------------------------------------
  // Modules
  //--------------------------------------------------------------------------------------------------

  rgmii_top
  #(
    . pBUF_SIZE     (2048      )
  )
  top__
  (
    .*
  );

endmodule
