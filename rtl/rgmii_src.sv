
//
// Project       : RGMII core
// Author        : Borchsh Vladislav
// Contacts      : borsch.v@micran.ru
// Workfile      : rgmii_src.sv
// Description   : Cross-clock buffer for input data
//

module rgmii_src
  #(
    parameter pWORDS = "ext"
  )
  (
    input                iclkin  ,
    input                iclkout ,
    input                irst    ,
    input                ireq    , // Request data from FIFO
    // Input packet (synced with iclkin)
    input                isop    ,
    input                iena    ,
    input          [7:0] idat    ,
    input                ieop    ,
    // Service (synced with iclkout)
    output               oerr    , // Overflow
    output               ordy    ,
    // Output packet (synced with iclkout)
    output logic         osop    ,
    output logic         oena    ,
    output logic   [7:0] odat    ,
    output logic         oeop
  );
  
  //--------------------------------------------------------------------------------------------------------
  // Declaration parameters
  //--------------------------------------------------------------------------------------------------------
  
  localparam int pWDAT = 8+1+1;
  localparam int pWADR = $clog2(pWORDS);
  localparam int pMARK = pWORDS * 4/5;
  localparam int pWCNT = $clog2(pWORDS/64);

  //--------------------------------------------------------------------------------------------------------
  // Declaration modules wires
  //--------------------------------------------------------------------------------------------------------
  
  logic   [pWDAT-1:0] fifo__data    ;
  logic               fifo__rdclk   ;
  logic               fifo__rdreq   ;
  logic               fifo__wrclk   ;
  logic               fifo__wrreq   ;
  logic               fifo__wrfull  ;
  logic               fifo__rdfull  ;
  logic   [pWDAT-1:0] fifo__q       ;
  logic               fifo__rdempty ;
  logic               fifo__wrempty ;
  logic               fifo__aclr    ;
  logic   [pWADR-1:0] fifo__wrusedw ;
  logic   [pWADR-1:0] fifo__rdusedw ;

  //--------------------------------------------------------------------------------------------------------
  // Declaration variables
  //--------------------------------------------------------------------------------------------------------
  logic                 rd_ena;
  logic                 rd_rdy;
  logic                 rd_dat;
  logic     [pWCNT-1:0] cnt;
  logic           [1:0] eof;
  logic           [1:0] feof;
  logic           [1:0] tail;
  logic           [1:0] feop;
  logic                 sop;
  logic                 preeop;
  logic                 eop;
  logic           [7:0] dat;
  logic                 deop;
  logic                 wr_ena;
  logic           [9:0] wr_dat;

  //--------------------------------------------------------------------------------------------------------
  // BODY
  //--------------------------------------------------------------------------------------------------------

  always_ff@(posedge iclkin) begin
    deop   <= 1'b0;//ieop; // one word more for correct enable signals
    wr_ena <= (isop | ieop | iena | deop);
    wr_dat <= (deop) ? ('0) : ({isop, ieop, idat});
  end

  always_ff@(posedge iclkin) begin
    if (ieop)         eop <= 1'b1;
    else if (eof[0])  eop <= 1'b0;
  end

  assign preeop = (fifo__q[8] & rd_rdy);

  always_ff@(posedge iclkout) begin
    eof[0] <= eop;
    rd_ena <= ireq & ~fifo__rdempty & (cnt > 0) & (~|tail) & ~preeop;
    rd_rdy <= rd_ena;
    rd_dat <= rd_rdy;

    if (eof[0])        feof[0] <= 1'b1;
    else if (feof[1])  feof[0] <= 1'b0;
    feof[1] <= feof[0] & ~feop[0];

    if (oeop)          feop[0] <= 1'b1;
    else if (feop[1])  feop[0] <= 1'b0;
    feop[1] <= feop[0];

    if (feop[1] & ~feop[0])       cnt <= (cnt > 0) ? (cnt - 1'b1) : (cnt);
    else if (feof[1] & ~feof[0])  cnt <= cnt + 1'b1;
    

    tail <= tail << 1 | preeop; // 2 ticks for mask oena signal
    sop  <= fifo__q[9] & rd_rdy;
  end

  // synthesis translate_off
  always_ff@(posedge iclkin) begin
    if (fifo__wrfull & fifo__wrreq) begin
      $display("!!! WARNING. FIFO OVERFLOW !!!");
      $display("%m");
    end
  end
  // synthesis translate_on

  //--------------------------------------------------------------------------------------------------------
  // Output data
  //--------------------------------------------------------------------------------------------------------
  
  assign ordy = fifo__wrempty | (fifo__wrusedw < pMARK);
  assign oerr = (fifo__wrfull & fifo__wrreq);

  always@(posedge iclkout) begin
    osop <= sop;
    oena <= (rd_rdy) & (~fifo__q[9]) & ~fifo__q[8] & (~|tail) & ~sop;
    oeop <= preeop;
    odat <= fifo__q[7:0];
  end
  // assign osop = sop;
  // assign oena = (rd_rdy) & ~fifo__q[9] & ~fifo__q[8] & (~|tail) & ~sop;
  // assign oeop = (fifo__q[8] & rd_rdy);
  // assign odat = fifo__q[7:0];
  
  //--------------------------------------------------------------------------------------------------------
  // Modules
  //--------------------------------------------------------------------------------------------------------

  assign fifo__aclr  = irst;
  assign fifo__wrclk = iclkin;
  assign fifo__wrreq = wr_ena;
  assign fifo__data  = wr_dat;
  assign fifo__rdclk = iclkout;
  assign fifo__rdreq = rd_ena;

  dcfifo
  #(
    . intended_device_family   ("Cyclone V"          ) ,
    . lpm_hint                 ("RAM_BLOCK_TYPE=M10K") ,
    . lpm_numwords             (pWORDS               ) ,
    . lpm_showahead            ("OFF"                ) ,
    . lpm_type                 ("dcfifo"             ) ,
    . lpm_width                (pWDAT                ) ,
    . lpm_widthu               (pWADR                ) ,
    . overflow_checking        ("ON"                 ) ,
    . rdsync_delaypipe         (4                    ) ,
    . read_aclr_synch          ("ON"                 ) ,
    . underflow_checking       ("ON"                 ) ,
    . use_eab                  ("ON"                 ) ,
    . write_aclr_synch         ("ON"                 ) ,
    . wrsync_delaypipe         (4                    )
  )
  fifo__
  (
    . aclr                     (fifo__aclr           ) ,
    . data                     (fifo__data           ) ,
    . rdclk                    (fifo__rdclk          ) ,
    . rdreq                    (fifo__rdreq          ) ,
    . wrclk                    (fifo__wrclk          ) ,
    . wrreq                    (fifo__wrreq          ) ,
    . q                        (fifo__q              ) ,
    . rdempty                  (fifo__rdempty        ) ,
    . wrfull                   (fifo__wrfull         ) ,
    . rdfull                   (fifo__rdfull         ) ,
    . rdusedw                  (fifo__rdusedw        ) ,
    . wrempty                  (fifo__wrempty        ) ,
    . wrusedw                  (fifo__wrusedw        ) ,
    . eccstatus                ()
  );

endmodule
