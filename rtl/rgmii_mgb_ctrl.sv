
//
// Project       : RGMII core
// Author        : Borchsh Vladislav
// Contacts      : borsch.v@micran.ru
// Workfile      : rgmii_mgb_ctrl.sv
// Description   : Control module of MGB interface of PHY
//

module rgmii_mgb_ctrl
  (
    input                iclk   , // 50 MHz max
    input                irst   ,
    //output    t_phy_info obus   ,
    //
    input                inint  ,
    output               ocfg   ,  
    output               ptp    ,
    output               omdc   ,
    inout                mdio   ,
    output               oreset
  );

  //--------------------------------------------------------------------------------------------------------
  // Declaration parameters
  //--------------------------------------------------------------------------------------------------------

  localparam pPHY_ADR = 5'd0 ;

`ifdef MODEL_TECH
  localparam pW_DELAY = 2 ;
`else
  localparam pW_DELAY = 15 ;
`endif

  //--------------------------------------------------------------------------------------------------------
  // Declaration variables
  //--------------------------------------------------------------------------------------------------------

  typedef struct packed
  {
    logic          ena   ;
    logic    [4:0] adr   ;
    logic   [15:0] dat   ;
  } t_this ;
  
  logic          [1:0] phy_speed  ;
  logic                phy_duplex ;
  logic                phy_link   ;
  logic                chng_param ;
  logic                reread     ;
  logic          [2:0] cnt_load   ;
  logic          [1:0] cnt_read   ;
  logic          [2:0] cnt_ena    ;
  logic         [15:0] regs       [32] ;
  logic   [pW_DELAY:0] cnt_init   ;
  logic                clk_ena    ;
  logic                clk_tx     ;
  t_this               wr_dat     [8] ;
  t_this               rd_dat     [4] ;
  t_this               rd_int     ;
  t_this               merge      ;
  
  enum int
  {
    StRst     = 0  ,
    StPause   = 1  ,
    StWrite   = 2  ,
    StWaitWr  = 3  ,
    StCheckWr = 4  ,
    StRead    = 5  ,
    StWaitRd  = 6  ,
    StCheckRd = 7  ,
    StIntRd   = 8  ,
    StWaitInt = 9  ,
    StIdle    = 10 
  } St;
  
  //--------------------------------------------------------------------------------------------------------
  // Initialization variables
  //--------------------------------------------------------------------------------------------------------
  
  initial begin
    St       = StRst;
    clk_tx   = '0;
    // synopsys translate_off
    cnt_load = '0;
    cnt_read = '0;
    cnt_ena  = '0;
    cnt_init = '0;
    // synopsys translate_on
  end
  
  //--------------------------------------------------------------------------------------------------------
  // Declaration modules wires
  //--------------------------------------------------------------------------------------------------------

  wire            mgb_rxtx__iclk       ;
  wire            mgb_rxtx__iclk_ena   ;
  wire            mgb_rxtx__iclk_tx    ;
  wire            mgb_rxtx__ird_ena    ;
  wire            mgb_rxtx__iwr_ena    ;
  wire      [4:0] mgb_rxtx__idev_adr   ;
  wire      [4:0] mgb_rxtx__ireg_adr   ;
  wire     [15:0] mgb_rxtx__idat       ;
  wire     [15:0] mgb_rxtx__odat       ;
  wire            mgb_rxtx__oeof       ;
  wire            mgb_rxtx__mdc        ;
  wire            mgb_rxtx__mdi        ;
  wire            mgb_rxtx__mdo        ;

  //--------------------------------------------------------------------------------------------------------
  // BODY
  //--------------------------------------------------------------------------------------------------------

  // Write
  assign wr_dat [ 0 ] = {1'b1, 5'd22, 8'h0, 8'h0}; // Page change (to "0")
  assign wr_dat [ 1 ] = {1'b1, 5'd18, {6'b011001, 6'h0, regs[18][3], 3'h0}}; // Set interrupts vector
  assign wr_dat [ 2 ] = {1'b1, 5'd22, 8'h0, 8'h2}; // Page change (to "2")
  assign wr_dat [ 3 ] = {1'b1, 5'd21, 16'h1060}; // Set TX clk delay
  assign wr_dat [ 4 ] = {1'b1, 5'd22, 8'h0, 8'd18}; // Page change (to "18")
  assign wr_dat [ 5 ] = {1'b1, 5'd20, 16'h8200}; // Set SW reset
  assign wr_dat [ 6 ] = {1'b0, 5'd22, 8'h0, 8'h0}; // reserve
  assign wr_dat [ 7 ] = {1'b0, 5'd22, 16'h0}; // reserve
  // Read
  // assign rd_dat = {1'b1, cnt_read, 16'h0} ;
  
  assign rd_dat [ 0 ] = {1'b1, 5'd17, 16'h0} ;
  assign rd_dat [ 1 ] = {1'b1, 5'd2 , 16'h0} ;
  assign rd_dat [ 2 ] = {1'b1, 5'd3 , 16'h0} ;
  assign rd_dat [ 3 ] = {1'b0, 5'd0 , 16'h0} ;
  // Interrupt
  assign rd_int       = {1'b1, 5'd19, 16'h0} ;
  
  // Make enable & output clock
  always@(posedge iclk) begin
    cnt_ena <= cnt_ena + 1'b1 ;
    
    clk_ena <= &cnt_ena ;
    
    if (&cnt_ena[1:0])  clk_tx <= ~clk_tx ;
  end
  
  always@(posedge iclk) begin
    if (clk_ena) begin
      if ( (St == StRst) || (St == StPause) )  cnt_init <= cnt_init + 1'b1 ;
      else                                     cnt_init <= '0 ;
    end
  end
  
  always@(posedge iclk) begin
    if (irst) begin
      cnt_load <= '0 ;
      cnt_read <= '0 ;
    end
    else if (clk_ena) begin
      if (St == StCheckWr)  cnt_load <= cnt_load + 1'b1;
      if (St == StCheckRd)  cnt_read <= cnt_read + 1'b1;
    end
  end
  
  always@(posedge iclk) begin
    if (clk_ena) begin
      case (St)
        // PHY reset
        StRst     : if (&cnt_init)       St <= StPause    ;
        // After reset or chng params pause
        StPause   : if (&cnt_init)       St <= StRead     ;
        // Start register reading
        StRead    :                      St <= StWaitRd   ;
        // Wait read one reg
        StWaitRd  : if (mgb_rxtx__oeof)  St <= StCheckRd  ;
                    else if (~merge.ena) St <= StCheckRd  ;
        // Check end of register reading
        StCheckRd : if (&cnt_read)       St <= (reread) ? (StIdle) : (StWrite) ;
                    else                 St <= StRead     ;
        // Start registers writing
        StWrite   :                      St <= StWaitWr   ;
        // Wait write one reg
        StWaitWr  : if (mgb_rxtx__oeof)  St <= StCheckWr  ;
                    else if (~merge.ena) St <= StCheckWr  ;
        // Check end of registers writing
        StCheckWr : if (&cnt_load)       St <= StIdle     ;
                    else                 St <= StWrite    ;
        // Read interrupt register
        StIntRd   :                      St <= StWaitInt  ;
        // Wait reading interrupt register
        StWaitInt : if (mgb_rxtx__oeof)  St <= StIdle     ;               
        // Wait interrupt or reset
        StIdle    : if (~inint)          St <= StIntRd    ;
                    else if (irst)       St <= StRst      ;
                      else if (reread)   St <= StPause    ;
      endcase
    end
  end
  
  always@(posedge iclk) begin
    if (St == StRead)          merge <= rd_dat[cnt_read] ;
    else if (St == StWrite)    merge <= wr_dat[cnt_load] ;
      else if (St == StIntRd)  merge <= rd_int ;
  end
  
  // Readed data handling
  always@(posedge iclk) begin
    if ( (St == StWaitRd) & mgb_rxtx__oeof)  regs[merge.adr] <= mgb_rxtx__odat ;
  end

  assign phy_speed  = regs[17][15:14] ;
  assign phy_duplex = regs[17][13] ;
  assign phy_link   = regs[17][10] ;
  
  // Interrupt handling
  assign chng_param = {   mgb_rxtx__odat[14]
                        ||mgb_rxtx__odat[13]
                        ||mgb_rxtx__odat[12]
                        ||mgb_rxtx__odat[10]
                      } ;

  always@(posedge iclk) begin
    if (mgb_rxtx__oeof & (St == StWaitInt))  reread <= chng_param ;
    else if (&cnt_read)                      reread <= 1'b0 ;
  end
  
  //--------------------------------------------------------------------------------------------------------
  // Modules
  //--------------------------------------------------------------------------------------------------------
  
  assign mgb_rxtx__iclk       = iclk;
  assign mgb_rxtx__iclk_ena   = clk_ena;
  assign mgb_rxtx__iclk_tx    = clk_tx;
  assign mgb_rxtx__ird_ena    = ( merge.ena & ((St == StRead) || (St == StIntRd)) ) ;
  assign mgb_rxtx__iwr_ena    = ( merge.ena & (St == StWrite) ) ;
  assign mgb_rxtx__idev_adr   = pPHY_ADR;
  assign mgb_rxtx__ireg_adr   = merge.adr;
  assign mgb_rxtx__idat       = merge.dat;
  assign mgb_rxtx__mdi        = mdio;
  
  rgmii_mgb_rxtx
  mgb_rxtx__
  (
    . iclk        (mgb_rxtx__iclk       ) ,
    . iclk_ena    (mgb_rxtx__iclk_ena   ) ,
    . iclk_tx     (mgb_rxtx__iclk_tx    ) ,
    . ird_ena     (mgb_rxtx__ird_ena    ) ,
    . iwr_ena     (mgb_rxtx__iwr_ena    ) ,
    . idev_adr    (mgb_rxtx__idev_adr   ) ,
    . ireg_adr    (mgb_rxtx__ireg_adr   ) ,
    . idat        (mgb_rxtx__idat       ) ,
    . odat        (mgb_rxtx__odat       ) ,
    . oeof        (mgb_rxtx__oeof       ) ,
    . mdc         (mgb_rxtx__mdc        ) ,
    . mdi         (mgb_rxtx__mdi        ) ,
    . mdo         (mgb_rxtx__mdo        )
  );

  //--------------------------------------------------------------------------------------------------------
  // Output data
  //--------------------------------------------------------------------------------------------------------

`ifndef MODEL_TECH
  //assign obus.speed  = phy_speed  ;
  //assign obus.duplex = phy_duplex ;
  //assign obus.link   = phy_link   ;
`else
  //assign obus.speed  = 2'b01 ;
  //assign obus.duplex = '1 ;
  //assign obus.link   = '1 ;
`endif
  assign ocfg   = 1'b1;
  assign ptp    = 1'bz;
  assign omdc   = mgb_rxtx__mdc;
  assign mdio   = mgb_rxtx__mdo;
  assign oreset = ~(St == StRst);
  
endmodule
