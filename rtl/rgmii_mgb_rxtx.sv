
//
// Project       : RGMII core
// Author        : Borchsh Vladislav
// Contacts      : borsch.v@micran.ru
// Workfile      : rgmii_mgb_rxtx.sv
// Description   : Transceiver module of MGB interface of PHY
//

module rgmii_mgb_rxtx
  (
    input             iclk       , // 50 MHz
    input             iclk_ena   , // 8.3 MHz MAX
    input             iclk_tx    , // 8.3 MHz MAX
    input             ird_ena    ,
    input             iwr_ena    ,
    input       [4:0] idev_adr   ,
    input       [4:0] ireg_adr   ,
    input      [15:0] idat       ,
    output     [15:0] odat       ,
    output            oeof       ,
    
    // PHY MGB interface
    output            mdc        , 
    input             mdi        ,
    output            mdo
  );
  
  //--------------------------------------------------------------------------------------------------------
  // Declaration variables
  //--------------------------------------------------------------------------------------------------------
  
  logic   [6:0] cnt     ;
  logic         rw_ena  ; // 1 - READ
  logic         start   ;
  logic         bitik   ;
  logic   [4:0] dev_adr ;
  logic   [4:0] reg_adr ;
  logic  [15:0] dat     ;

  enum int
  {
    StIdle     = 0 ,
    StPreamble = 1 ,
    StSOF      = 2 ,
    StOpC      = 3 ,
    StDevAdr   = 4 ,
    StRegAdr   = 5 ,
    StTA       = 6 ,
    StData     = 7 ,
    StEOF      = 8
  } St;
  
  //--------------------------------------------------------------------------------------------------------
  // Declaration modules wires
  //--------------------------------------------------------------------------------------------------------

  wire       ddio__iclk   ;
  wire       ddio__idat_h ;
  wire       ddio__idat_l ;
  wire       ddio__odat   ;

  //--------------------------------------------------------------------------------------------------------
  // Initialization variables
  //--------------------------------------------------------------------------------------------------------

  initial begin
    St = StIdle ;
  end
  
  //--------------------------------------------------------------------------------------------------------
  // BODY
  //--------------------------------------------------------------------------------------------------------

  assign start = (ird_ena || iwr_ena) ;

  always@(posedge iclk) begin  
    if (iclk_ena) begin
      if (start)       cnt <= '0 ;
      else if (~&cnt)  cnt <= cnt + 1'b1 ;
      
      if (start)       rw_ena <= ird_ena ;
    end
  end

  always@(posedge iclk) begin
    if (iclk_ena) begin
      case (St)
        StIdle     : if (start)      St <= StPreamble ;
        StPreamble : if (cnt == 31)  St <= StSOF      ;
        StSOF      : if (cnt == 33)  St <= StOpC      ;
        StOpC      : if (cnt == 35)  St <= StDevAdr   ;
        StDevAdr   : if (cnt == 40)  St <= StRegAdr   ;
        StRegAdr   : if (cnt == 45)  St <= StTA       ;
        StTA       : if (cnt == 47)  St <= StData     ;
        StData     : if (cnt == 62)  St <= StEOF      ;
        StEOF      : if (cnt == 63)  St <= StIdle     ;
      endcase
    end
  end

  always@(posedge iclk) begin
    if (iclk_ena) begin  
      if (start)                dev_adr <= idev_adr ;
      else if (St == StDevAdr)  dev_adr <= dev_adr << 1 ;
      
      if (start)                reg_adr <= ireg_adr ;
      else if (St == StRegAdr)  reg_adr <= reg_adr << 1 ;
      
      if (start)                dat     <= idat ;
      else if (St == StData)    dat     <= dat << 1 | mdi ;
    end
  end
  
  // Output data commutation
  always_comb begin
    case (St)
      StIdle     : bitik = 1'bZ ;
      StPreamble : bitik = 1'b1 ;
      StSOF      : bitik = (cnt[0]) ;
      StOpC      : bitik = (cnt[0]) ^ rw_ena ;
      StDevAdr   : bitik = dev_adr[$high(dev_adr)] ;
      StRegAdr   : bitik = reg_adr[$high(reg_adr)] ;
      StTA       : bitik = (rw_ena) ? (1'bZ) : (!cnt[0]) ;
      StData     : bitik = (rw_ena) ? (1'bZ) : (dat[$high(dat)]) ;
      StEOF      : bitik = 1'bZ ;
    endcase
  end
  
  //--------------------------------------------------------------------------------------------------------
  // Modules
  //--------------------------------------------------------------------------------------------------------
  
  assign ddio__iclk   = iclk_tx ;
  assign ddio__idat_h = 1'b1    ;
  assign ddio__idat_l = 1'b0    ;

  altddio_out
  #(
    . extend_oe_disable     ("UNUSED"         ) ,
    . lpm_type              ("altddio_out"    ) ,
    . oe_reg                ("UNUSED"         ) ,
    . power_up_high         ("OFF"            ) ,
    . width                 (1                )
  )                           	
	ddio__
  (                           	
    . outclock 					    (ddio__iclk       ) ,
    . datain_h 					    (ddio__idat_h     ) ,
    . datain_l 					    (ddio__idat_l     ) ,
    . dataout 					    (ddio__odat       ) ,
    . outclocken 				    (1'b1) ,
    . oe 						        (1'b1) ,
    . aset                  (    ) ,
    . aclr                  (    ) ,
    . sset                  (    ) ,
    . sclr                  (    ) ,
    . oe_out                (    )
  );

  //--------------------------------------------------------------------------------------------------------
  // Output data
  //--------------------------------------------------------------------------------------------------------

  assign oeof = (St == StEOF) ;
  assign odat = dat ;
  
  assign mdc  = ddio__odat ;
  assign mdo  = bitik ;
  
endmodule
