`ifndef __ETH_RGMII_H__
`define __ETH_RGMII_H__

  //***************************************************************************************************
  // Types
  //***************************************************************************************************
  
  interface rgmii_if();
    // tx
    logic           txclk;
    logic     [3:0] txd;
    logic           txctrl;
    // rx
    logic           clk;
    logic           rxclk;
    logic           rxctrl;
    logic     [3:0] rxd;
    // ctrl
    logic           reset; 
    logic           cfg;
    logic           nint;
    wire            mdio;
    logic           mdc;
    wire            ptp;
    
    modport tx
    (
      output    txclk  ,
      output    txd    ,
      output    txctrl
    );
    
    modport rx
    (
      input     rxclk  ,
      input     rxctrl ,
      input     rxd
    );
    
    modport ctrl
    (
      output    cfg    ,
      output    reset  ,
      output    mdc    ,
      inout     mdio   ,
      inout     ptp    ,
      input     nint
    );
  endinterface : rgmii_if

  //***************************************************************************************************
  // Files connection
  //***************************************************************************************************

  `include "./rtl/rgmii_mgb_ctrl.sv"
  `include "./rtl/rgmii_mgb_rxtx.sv"
  `include "./rtl/rgmii_rx.sv"
  `include "./rtl/rgmii_src.sv"
  `include "./rtl/rgmii_sink.sv"
  `include "./rtl/rgmii_top.sv"
  `include "./rtl/rgmii_tx.sv"


`endif